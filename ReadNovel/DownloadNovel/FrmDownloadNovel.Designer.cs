﻿namespace DownloadNovel
{
    partial class FrmDownloadNovel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDownloadNovel));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbState = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp1Start = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.btn1Start = new System.Windows.Forms.Button();
            this.txtStart = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tp2Catalog = new System.Windows.Forms.TabPage();
            this.dgvCatalog = new System.Windows.Forms.DataGridView();
            this.title1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.url1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDown = new System.Windows.Forms.Button();
            this.tp3Paser = new System.Windows.Forms.TabPage();
            this.rtSection = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnGet = new System.Windows.Forms.Button();
            this.tp4Save = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnTotal = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tp1Start.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tp2Catalog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCatalog)).BeginInit();
            this.panel2.SuspendLayout();
            this.tp3Paser.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tp4Save.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lbState});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(884, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(68, 17);
            this.toolStripStatusLabel1.Text = "当前状态：";
            // 
            // lbState
            // 
            this.lbState.ForeColor = System.Drawing.Color.Red;
            this.lbState.Name = "lbState";
            this.lbState.Size = new System.Drawing.Size(36, 17);
            this.lbState.Text = "       ";
            this.lbState.ToolTipText = "双击复制信息";
            this.lbState.DoubleClick += new System.EventHandler(this.lbState_DoubleClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tp1Start);
            this.tabControl1.Controls.Add(this.tp2Catalog);
            this.tabControl1.Controls.Add(this.tp3Paser);
            this.tabControl1.Controls.Add(this.tp4Save);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(884, 540);
            this.tabControl1.TabIndex = 1;
            // 
            // tp1Start
            // 
            this.tp1Start.Controls.Add(this.panel1);
            this.tp1Start.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tp1Start.Location = new System.Drawing.Point(4, 22);
            this.tp1Start.Name = "tp1Start";
            this.tp1Start.Padding = new System.Windows.Forms.Padding(3);
            this.tp1Start.Size = new System.Drawing.Size(876, 514);
            this.tp1Start.TabIndex = 0;
            this.tp1Start.Text = "   1.开  始   ";
            this.tp1Start.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.btnTotal);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btn1Start);
            this.panel1.Controls.Add(this.txtStart);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(870, 508);
            this.panel1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Purple;
            this.label4.Location = new System.Drawing.Point(18, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(744, 190);
            this.label4.TabIndex = 5;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // btn1Start
            // 
            this.btn1Start.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn1Start.Location = new System.Drawing.Point(600, 10);
            this.btn1Start.Name = "btn1Start";
            this.btn1Start.Size = new System.Drawing.Size(88, 38);
            this.btn1Start.TabIndex = 4;
            this.btn1Start.Text = " 开 始 ";
            this.btn1Start.UseVisualStyleBackColor = true;
            this.btn1Start.Click += new System.EventHandler(this.btn1Start_Click);
            // 
            // txtStart
            // 
            this.txtStart.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtStart.Location = new System.Drawing.Point(109, 15);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(479, 29);
            this.txtStart.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(8, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "目录网址：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(-1, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1289, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(321, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = " 小 说 下 载 器\r\n        -- Mr_Yang";
            // 
            // tp2Catalog
            // 
            this.tp2Catalog.Controls.Add(this.dgvCatalog);
            this.tp2Catalog.Controls.Add(this.panel2);
            this.tp2Catalog.Location = new System.Drawing.Point(4, 22);
            this.tp2Catalog.Name = "tp2Catalog";
            this.tp2Catalog.Padding = new System.Windows.Forms.Padding(3);
            this.tp2Catalog.Size = new System.Drawing.Size(876, 514);
            this.tp2Catalog.TabIndex = 1;
            this.tp2Catalog.Text = "  2.显示章节列表  ";
            this.tp2Catalog.UseVisualStyleBackColor = true;
            // 
            // dgvCatalog
            // 
            this.dgvCatalog.AllowUserToAddRows = false;
            this.dgvCatalog.AllowUserToDeleteRows = false;
            this.dgvCatalog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCatalog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCatalog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.title1,
            this.url1});
            this.dgvCatalog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCatalog.Location = new System.Drawing.Point(3, 66);
            this.dgvCatalog.Name = "dgvCatalog";
            this.dgvCatalog.RowTemplate.Height = 23;
            this.dgvCatalog.Size = new System.Drawing.Size(870, 445);
            this.dgvCatalog.TabIndex = 1;
            this.dgvCatalog.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgGrid_RowPostPaint);
            // 
            // title1
            // 
            this.title1.HeaderText = "章节名";
            this.title1.Name = "title1";
            this.title1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.title1.Width = 47;
            // 
            // url1
            // 
            this.url1.HeaderText = "网址";
            this.url1.Name = "url1";
            this.url1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.url1.Width = 35;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.btnDown);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(870, 63);
            this.panel2.TabIndex = 0;
            // 
            // btnDown
            // 
            this.btnDown.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDown.Location = new System.Drawing.Point(600, 10);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(88, 38);
            this.btnDown.TabIndex = 5;
            this.btnDown.Text = " 下 载 ";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // tp3Paser
            // 
            this.tp3Paser.Controls.Add(this.rtSection);
            this.tp3Paser.Controls.Add(this.panel3);
            this.tp3Paser.Location = new System.Drawing.Point(4, 22);
            this.tp3Paser.Name = "tp3Paser";
            this.tp3Paser.Size = new System.Drawing.Size(876, 514);
            this.tp3Paser.TabIndex = 3;
            this.tp3Paser.Text = "   3.显示前几章   ";
            this.tp3Paser.UseVisualStyleBackColor = true;
            // 
            // rtSection
            // 
            this.rtSection.BackColor = System.Drawing.SystemColors.Window;
            this.rtSection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtSection.Location = new System.Drawing.Point(0, 63);
            this.rtSection.Name = "rtSection";
            this.rtSection.Size = new System.Drawing.Size(876, 451);
            this.rtSection.TabIndex = 2;
            this.rtSection.Text = "";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.btnGet);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(876, 63);
            this.panel3.TabIndex = 1;
            // 
            // btnGet
            // 
            this.btnGet.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnGet.Location = new System.Drawing.Point(600, 10);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(88, 38);
            this.btnGet.TabIndex = 5;
            this.btnGet.Text = " 获 取 ";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // tp4Save
            // 
            this.tp4Save.Controls.Add(this.panel4);
            this.tp4Save.Location = new System.Drawing.Point(4, 22);
            this.tp4Save.Name = "tp4Save";
            this.tp4Save.Size = new System.Drawing.Size(876, 514);
            this.tp4Save.TabIndex = 4;
            this.tp4Save.Text = "   4.保存成TXT   ";
            this.tp4Save.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.btnOpen);
            this.panel4.Controls.Add(this.txtPath);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(876, 63);
            this.panel4.TabIndex = 2;
            // 
            // txtPath
            // 
            this.txtPath.BackColor = System.Drawing.SystemColors.Window;
            this.txtPath.Location = new System.Drawing.Point(118, 19);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(476, 21);
            this.txtPath.TabIndex = 7;
            this.txtPath.Click += new System.EventHandler(this.txtPath_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(8, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 19);
            this.label5.TabIndex = 6;
            this.label5.Text = "保存地址：";
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSave.Location = new System.Drawing.Point(600, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(88, 38);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = " 保 存 ";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnTotal
            // 
            this.btnTotal.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnTotal.Location = new System.Drawing.Point(600, 65);
            this.btnTotal.Name = "btnTotal";
            this.btnTotal.Size = new System.Drawing.Size(173, 38);
            this.btnTotal.TabIndex = 6;
            this.btnTotal.Text = " 一键获取 ";
            this.btnTotal.UseVisualStyleBackColor = true;
            this.btnTotal.Click += new System.EventHandler(this.btnTotal_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOpen.Location = new System.Drawing.Point(710, 15);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(120, 29);
            this.btnOpen.TabIndex = 8;
            this.btnOpen.Text = "打开文件夹";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // FrmDownloadNovel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "FrmDownloadNovel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "小说下载器";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmDownloadNovel_FormClosed);
            this.Load += new System.EventHandler(this.FrmDownloadNovel_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tp1Start.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tp2Catalog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCatalog)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tp3Paser.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tp4Save.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp1Start;
        private System.Windows.Forms.TabPage tp2Catalog;
        private System.Windows.Forms.TabPage tp3Paser;
        private System.Windows.Forms.TabPage tp4Save;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn1Start;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgvCatalog;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lbState;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RichTextBox rtSection;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn title1;
        private System.Windows.Forms.DataGridViewTextBoxColumn url1;
        private System.Windows.Forms.Button btnTotal;
        private System.Windows.Forms.Button btnOpen;
    }
}