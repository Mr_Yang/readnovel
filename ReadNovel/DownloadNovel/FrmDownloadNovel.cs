﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ReadNovelDll;
using System.Threading;
using System.IO;

namespace DownloadNovel
{
    public partial class FrmDownloadNovel : Form
    {
        public FrmDownloadNovel()
        {
            InitializeComponent();
        }

        private void FrmDownloadNovel_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            txtPath.Text = AppDomain.CurrentDomain.BaseDirectory + @"小说.txt";
        }

        static NovelEntity novel;

        /// <summary>
        /// 1.开始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn1Start_Click(object sender, EventArgs e)
        {
            //判断地址是否正确
            string strUrl = txtStart.Text.Trim();
            strUrl = urlCheck(strUrl);
            if (strUrl == "")
            {
                MessageBox.Show("开始地址格式不正确！");
                return;
            }
            else
            {
                txtStart.Text = strUrl;
            }
            try
            {

                novel = new NovelEntity(strUrl);
                novel.ParserCatalog();
                ShowCatalog();
                lbState.Text = "1.显示章节列表成功！";
            }
            catch (Exception ex)
            {
                MesError("1.开始", ex);
            }
        }


        //判断地址是否正确
        private string urlCheck(string strUrl)
        {
            //if (strUrl.IndexOf("://") < 0) strUrl = "http://" + strUrl;

            //匹配Url的正则表达式：
            //  ^[a-zA-z]+://[^\s]*$
            //  (https?://)?([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*/?
            //  [a-zA-z]+://([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*/?
            string regUrl = @"^[a-zA-z]+://([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*/?$";
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(regUrl);
            System.Text.RegularExpressions.Match mc = reg.Match(strUrl);
            if (mc.Length <= 0)
            {
                strUrl = "http://" + strUrl;
            }

            try
            {
                Uri i = new Uri(strUrl);
                return i.ToString();
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 2.展示章节及网址
        /// </summary>
        private void ShowCatalog()
        {
            dgvCatalog.Rows.Clear();

            foreach (var item in novel.Catalogs)
            {
                dgvCatalog.Rows.Add(item.title, item.Url);
            }

            tabControl1.SelectTab(tp2Catalog);
        }

        private void lbState_DoubleClick(object sender, EventArgs e)
        {
            string msg = lbState.Text.Trim();
            if (msg != "")
            {
                Clipboard.SetText(msg);
                MessageBox.Show("信息已复制到剪切板！");
            }
        }


        private void MesError(string locate, Exception ex)
        {
            string msg = "出现错误,发生在【" + locate + "】。\n请双击状态框复制信息！"
                +"\n网址："+txtStart.Text
                + "\nMessage:" + ex.Message + "\nInnerException:" + ex.InnerException;
            lbState.Text = msg;
            MessageBox.Show(msg);
        }

        /// <summary>
        /// 2.下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDown_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tp3Paser);
            Thread t = new Thread(new ParameterizedThreadStart(DownLoad));
            btnDown.Enabled = false;
            lbState.Text = "2.下载中。。。。。";
            t.Start();
        }

        void DownLoad(object obj)
        {
            try
            {
                novel.Download();
                novel.CreateSigns();
                lbState.Text = "2.下载完成！";

                rtSection.Clear();
                for (int i = 0; i < 3; i++)
                {
                    string str = novel.GetSectionByIndex(i);
                    rtSection.AppendText(str);
                }
                lbState.Text = "2.下载完成并显示前三章！";
            }
            catch (Exception ex)
            {
                MesError("2.下载", ex);
            }
            finally
            {
                btnDown.Enabled = true;

            }
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tp4Save);
            Thread t = new Thread(new ParameterizedThreadStart(GetContent));
            btnGet.Enabled = false;
            lbState.Text = "3.获取中。。。。。";
            t.Start();
        }

        void GetContent(object obj)
        {
            try
            {                
                novel.GetSection();
                novel.DealErrorCode();
                lbState.Text = "3.获取完成！";          
            }
            catch (Exception ex)
            {
                MesError("3.获取", ex);
            }
            finally
            {
                btnGet.Enabled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            novel.SaveTXT(txtPath.Text);
            MessageBox.Show("提示", "保存成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void txtPath_Click(object sender, EventArgs e)
        {
            SaveFileDialog s = new SaveFileDialog();
            s.Filter = "文本文件(*.txt)|*.txt|所有文件|*.*";//设置文件类型
            s.FileName = "小说";//设置默认文件名
            s.DefaultExt = "txt";//设置默认格式（可以不设）
            s.RestoreDirectory = true;
            s.FileName = txtPath.Text;
            if (s.ShowDialog()==DialogResult.OK)
            {
                txtPath.Text = s.FileName;
            }
        }



        /// <summary>
        /// 绘制行号
        /// 来自：https://blog.csdn.net/xieyufei/article/details/9769631
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgGrid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }

        private void FrmDownloadNovel_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void btnTotal_Click(object sender, EventArgs e)
        {
            try
            {
                btn1Start_Click(null, null);
                btnDown_Click(null, null);
                btnGet_Click(null, null);
            }
            catch (Exception ex)
            {
                
                  MesError("1.一键获取", ex);
            }
           
        }

        /// <summary>
        /// 打开文件夹
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpen_Click(object sender, EventArgs e)
        {
            string path = txtPath.Text.Trim();
            if (File.Exists(path))
            {
                System.Diagnostics.Process.Start("explorer.exe", "/select," + path);                
            }
            else
            {
                System.Diagnostics.Process.Start("explorer.exe", Path.GetDirectoryName(path));
            }
            
        }
    }
}
