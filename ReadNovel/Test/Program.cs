﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReadNovelDll;
using System.Threading;
using System.Net;
using System.IO;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = 512;
            //TestXML();
            //CreateXML();
            //TestThread();
            //string str = NovelO.WebClientGetGeneralContent(@"https://www.bxwx9.org/b/5/5740/41270028.html");
            //Console.WriteLine(str);

            //TestHtmlD();

            //TestDownload();

            //TestParser();

            //TestEncode();

            OfficialVersion();

            Console.WriteLine("成功");
            Console.ReadKey();
        }
        public static void OfficialVersion()
        {
            int start = System.Environment.TickCount;

            //novel = new NovelEntity(@"https://www.bqg5200.com/xiaoshuo/29/29993/");            

            novel = new NovelEntity(@"https://www.bqg5200.com/xiaoshuo/15/15248/");

            //novel = new NovelEntity(@"https://www.bxwx9.org/b/5/5740/index.html");

            novel.ParserCatalog();
            novel.Download();

            int end = System.Environment.TickCount;

            Console.WriteLine((end - start) / 1000.0);
            Console.WriteLine("OK");
            //XmlO.ToXMLFile(novel);
        }


        public static void TestHtmlD()
        {
            string url = @"https://www.bxwx9.org/b/5/5740/41214369.html";
            string s = NovelO.WebClientGetGeneralContent(url);
            Console.WriteLine(s);

            Console.WriteLine("-----------------------------------------");

            WebRequest request = WebRequest.Create(url); //WebRequest.Create方法，返回WebRequest的子类HttpWebRequest
            WebResponse response = request.GetResponse(); //WebRequest.GetResponse方法，返回对 Internet 请求的响应
            Stream resStream = response.GetResponseStream(); //WebResponse.GetResponseStream 方法，从 Internet 资源返回数据流。
            Encoding enc = Encoding.GetEncoding("utf-8"); // 如果是乱码就改成 utf-8 / GB2312
            StreamReader sr = new StreamReader(resStream, enc); //命名空间:System.IO。 StreamReader 类实现一个 TextReader (TextReader类，表示可读取连续字符系列的读取器)，使其以一种特定的编码从字节流中读取字符。
            s = sr.ReadToEnd(); //输出(HTML代码)
            Console.WriteLine(s);
        }

        public static void TestXML()
        {
            string name = "20180811one.xml";
            //测试类转xml
            NovelEntity n = new NovelEntity() { Url = "cs2", WebSource = "html-124sssssssss" };
            XmlO.ToXMLFile(n, name);
            Console.WriteLine("成功");
            //测试xml转类
            NovelEntity n2 = XmlO.ToEntity<NovelEntity>(name);
            Console.WriteLine(n2.ToString());
            Console.WriteLine("成功");
        }

        private static AutoResetEvent _event;
        public static void TestThread()
        {
            _event = new AutoResetEvent(false);
            novel = GetEntity();
            for (int i = 0; i < 12; i++)
            {
                novel.Catalogs.RemoveAt(1);
            }
            Console.WriteLine("开始");
            for (int i = 0; i < novel.Catalogs.Count; i++)
            {
                System.Threading.ThreadPool.QueueUserWorkItem(TestMethod_new, i);
            }
            _event.WaitOne();
            //WaitHandle.WaitAll(manualEvents.ToArray());
            Console.WriteLine("完成");
            for (int i = 0; i < novel.Catalogs.Count; i++)
            {
                Console.WriteLine(i.ToString()+":"+novel.Catalogs[i].IsDownload.ToString());
            }
        }

        public static void TestMethod(object i)
        {
            string strUrl = novel.Catalogs[(int)i].Url;
            System.Net.WebClient MyWebClient = new System.Net.WebClient();
            MyWebClient.Credentials = System.Net.CredentialCache.DefaultCredentials;//获取或设置用于向Internet资源的请求进行身份验证的网络凭据
            Byte[] pageData = MyWebClient.DownloadData(strUrl); //从指定网站下载数据
            string strMsg = Encoding.Default.GetString(pageData);  //如果获取网站页面采用的是GB2312，则使用这句
            novel.Catalogs[(int)i].SectionSnource = strMsg;
            novel.Catalogs[(int)i].IsDownload = true;
        }
        public static void TestMethod_new(object i)
        {
            string strUrl = novel.Catalogs[(int)i].Url;
            //Console.WriteLine(strUrl);

            string strMsg = NovelO.WebClientGetGeneralContent(strUrl);
            //Console.WriteLine(strMsg);
            novel.Catalogs[(int)i].SectionSnource = strMsg;
            novel.Catalogs[(int)i].IsDownload = true;
            if ((int)i >=novel.Catalogs.Count-1)
            {
                _event.Set();
            }
        }
        public static NovelEntity novel;
        public static void TestThread1()
        {
            NovelEntity n = GetEntity();
            //将工作项加入到线程池队列中，这里可以传递一个线程参数
            System.Threading.ThreadPool.QueueUserWorkItem(TestMethod1, "Hello");
            System.Threading.ThreadPool.QueueUserWorkItem(TestMethod1, "Hello1");
            System.Threading.ThreadPool.QueueUserWorkItem(TestMethod1, "Hello2");
            Console.ReadKey();
        }

        public static void TestMethod1(object data)
        {
            string datastr = data as string;
            Console.WriteLine(datastr);
        }

        public static void CreateXML()
        {
            NovelEntity n = new NovelEntity()
            {
                Url = "https://www.bxwx9.org/b/5/5740/index.html",
                WebSource = "",
                Catalogs = new List<Catalog>()
                {
                    new Catalog(){Id=1,Url = "https://www.bxwx9.org/b/5/5740/41214369.html",title = "正文 第四章 源纹的力量"},
                    new Catalog(){Id=2,Url = "https://www.bxwx9.org/b/5/5740/41209550.html",title = "正文 第一章 蟒雀吞龙"},
                    new Catalog(){Id=3,Url = "https://www.bxwx9.org/b/5/5740/41212429.html",title = "正文 第三章  苏幼微"},
                    new Catalog(){Id=4,Url = "https://www.bxwx9.org/b/5/5740/41211210.html",title = "正文 第二章  源纹"},
                    new Catalog(){Id=5,Url = "https://www.bxwx9.org/b/5/5740/41242812.html",title = "正文 第八章 寻八脉"},
                    new Catalog(){Id=6,Url = "https://www.bxwx9.org/b/5/5740/41217736.html",title = "正文 第五章   齐岳，柳溪"},
                    new Catalog(){Id=7,Url = "https://www.bxwx9.org/b/5/5740/41240919.html",title = "正文 第七章 神秘之地"},
                    new Catalog(){Id=8,Url = "https://www.bxwx9.org/b/5/5740/41226562.html",title = "正文 第六章  祖地宗祠"},
                    new Catalog(){Id=9,Url = "https://www.bxwx9.org/b/5/5740/41257573.html",title = "正文 第十二章  源食"},
                    new Catalog(){Id=10,Url = "https://www.bxwx9.org/b/5/5740/41249369.html",title = "正文 第九章  八脉现"},
                    new Catalog(){Id=11,Url = "https://www.bxwx9.org/b/5/5740/41253489.html",title = "正文 第十一章  齐王的胃口"},
                    new Catalog(){Id=12,Url = "https://www.bxwx9.org/b/5/5740/41250996.html",title = "正文 第十章 得授机缘"},
                    new Catalog(){Id=13,Url = "https://www.bxwx9.org/b/5/5740/41271359.html",title = "正文 第十六章 挖墙角"},
                    new Catalog(){Id=14,Url = "https://www.bxwx9.org/b/5/5740/41263127.html",title = "正文 第十三章  冲脉"},
                    new Catalog(){Id=15,Url = "https://www.bxwx9.org/b/5/5740/41270028.html",title = "正文 第十五章   混沌神磨观想法"},
                    new Catalog(){Id=16,Url = "https://www.bxwx9.org/b/5/5740/41264625.html",title = "正文 第十四章  神魂"},
                    new Catalog(){Id=17,Url = "https://www.bxwx9.org/b/5/5740/41292130.html",title = "正文 第二十章  源术"},
                    new Catalog(){Id=18,Url = "https://www.bxwx9.org/b/5/5740/41278028.html",title = "正文 第十七章  林枫"},
                    new Catalog(){Id=19,Url = "https://www.bxwx9.org/b/5/5740/41289426.html",title = "正文 第十九章  开脉"},
                    new Catalog(){Id=20,Url = "https://www.bxwx9.org/b/5/5740/41282432.html",title = "正文 第十八章  文武"},
                    new Catalog(){Id=21,Url = "https://www.bxwx9.org/b/5/5740/41306078.html",title = "正文 第二十四章 显威"},
                    new Catalog(){Id=22,Url = "https://www.bxwx9.org/b/5/5740/41296551.html",title = "正文 第二十一章  龙步，龙碑手"},
                    new Catalog(){Id=23,Url = "https://www.bxwx9.org/b/5/5740/41303664.html",title = "正文 第二十三章  初露峥嵘"}
                }
            };
            string name = "测试.xml";
            XmlO.ToXMLFile(n, name);
            Console.WriteLine("成功");
        }

        public static NovelEntity GetEntity()
        {
            return (NovelEntity)XmlO.ToEntity<NovelEntity>("测试.xml");
        }

        public static void TestDownload()
        {
            novel = GetEntity();
            novel.Download();
            Console.WriteLine(novel.Undown);
            XmlO.ToXMLFile(novel);
        }

        public static void TestParser()
        {
            novel = new NovelEntity(@"https://www.bqg5200.com/xiaoshuo/29/29993/");
            novel = new NovelEntity(@"https://www.bxwx9.org/b/5/5740/41214369.html");
       
            novel.ParserCatalog();
            Console.WriteLine("OK");
            XmlO.ToXMLFile(novel);
        }


        public static void TestEncode()
        {
            //novel = new NovelEntity(@"https://www.bqg5200.com/xiaoshuo/2/2893/");
            novel = new NovelEntity(@"https://www.bqg5200.com/xiaoshuo/29/29993/");
            novel = new NovelEntity(@"http://www.msdn.net/");
            novel = new NovelEntity(@"http://www.cnblogs.com/");


            //novel.ParserCatalog();
            Console.WriteLine("OK");
            //XmlO.ToXMLFile(novel);
        }
    }
}
