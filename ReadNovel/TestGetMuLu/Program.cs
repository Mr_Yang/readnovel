﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace TestGetMuLu
{
    class Program
    {
        static void Main(string[] args)
        {
            test2();
            Console.ReadKey();
        }


        public static void test1()
        {
            string text = WebClientGetGeneralContent(@"https://www.bqg5200.com/xiaoshuo/2/2893/");
            //string text = File.ReadAllText(Environment.CurrentDirectory + "//test.txt", Encoding.GetEncoding("gb2312"));
            string prttern = "<a(\\s+(href=\"(?<url>([^\"])*)\"|'([^'])*'|\\w+=\"(([^\"])*)\"|'([^'])*'))+>(?<text>(.*?))</a>";
            var maths = Regex.Matches(text, prttern);
            //抓取出来写入的文件
            using (FileStream w = new FileStream(Environment.CurrentDirectory + "//wirter.txt", FileMode.Create))
            {

                for (int i = 0; i < maths.Count; i++)
                {
                    byte[] bs = Encoding.UTF8.GetBytes(string.Format("链接地址:{0},   innerhtml：{1}", maths[i].Groups["url"].Value,
                        maths[i].Groups["text"].Value) + "\r\n");
                    w.Write(bs, 0, bs.Length);
                    Console.WriteLine();
                }
            }
            Console.ReadKey();
        }

        public static void test2()
        {
            Uri u = new Uri(@"https://www.bqg5200.com/xiaoshuo/2/2893/");
            Uri u1 = new Uri(u, @"/xiaoshuo/2/2893/1637168.html");
            Uri u2 = new Uri(u, @"https://www.bqg5200.com/all.html");
            string s = u1.GetLeftPart(UriPartial.Authority);
            Uri de = u.MakeRelativeUri(u1);

            Uri de2 = u.MakeRelativeUri(u2);
            bool b = u.MakeRelativeUri(u1).IsAbsoluteUri;
            b = u.IsBaseOf(u1);

            int i  =Uri.Compare(u, u1, UriComponents.Path, UriFormat.SafeUnescaped, StringComparison.Ordinal);

        }

        public static string WebClientGetGeneralContent(string strUrl)
        {
            System.Net.WebClient MyWebClient = new System.Net.WebClient();


            MyWebClient.Credentials = System.Net.CredentialCache.DefaultCredentials;//获取或设置用于向Internet资源的请求进行身份验证的网络凭据

            Byte[] pageData = MyWebClient.DownloadData(strUrl); //从指定网站下载数据

            string strMsg = Encoding.Default.GetString(pageData);  //如果获取网站页面采用的是GB2312，则使用这句
            //string strMsg = Encoding.UTF8.GetString(pageData); //如果获取网站页面采用的是UTF-8，则使用这句
            return strMsg;
        }
    }
}
