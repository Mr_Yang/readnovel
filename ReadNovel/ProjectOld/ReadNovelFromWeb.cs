﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace ReadNovel
{
    //结构体，存储章
    public struct Novel
    {
        public string Title;
        public string Content;

        public void Empty()
        {
            Title = null;
            Content = null;
        }
    }

    //抓取类
    public class ReadNovelFromWeb
    {
        //提取内容的正则表达式
        private string regTitle;//章节名
        private string regContent;//内容
        private string regNextWeb;//下一章
        private string regEnter = "<br />";//换行
        private string regSpace = "&nbsp;";//空格
        public string startWeb;//开始页
        public string nowWeb;//当前页
        public string nextWeb;//下一页
        public string endWeb;//结束页
        public Novel novel;

        public int isExistINI = 1;
        //0表示不存在，1表示存在
        //用来标识是否存在该网址的配置
        

        public ReadNovelFromWeb()
        {
            //"http://www.bxwx.org/b/62/62724/11455540.html";
            //bxwx.org
            regTitle = "<div id=\"title\">(.*)</div>";
            regContent = "<div id=\"content\"><div id=\"adright\"></div>(.*)</div>";
            regNextWeb = "<li><a href=\"(.*?)\">下一页</a></li>";

        }

        public ReadNovelFromWeb(string strUrl)
        {
            Uri url = new Uri(strUrl);
            string section = url.Host;
            string file = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "Settings.ini";
            if (!System.IO.File.Exists(file)) operateIni.INIOperationClass.WriteINI();

            regContent = operateIni.INIOperationClass.INIGetStringValue(file, section, "Content", null);
            if (regContent == "" || regContent == null)
            {
                this.isExistINI = 0;
                return;
            }
            regTitle = operateIni.INIOperationClass.INIGetStringValue(file, section, "Title", null);
            regNextWeb = operateIni.INIOperationClass.INIGetStringValue(file, section, "NextWeb", null);

            this.startWeb = strUrl;
            this.nextWeb = this.startWeb;
        }

        public void GetNovel(string strUrl)
        {
            string strMsg = this.WebClientGetGeneralContent(strUrl);
            novel.Title = this.GetTitle(strMsg);
            string temp = this.GetContent(strMsg);
            novel.Content = this.ReplaceContent(temp);

            this.nextWeb = this.GetNextWeb(strMsg);//需修改

        }
        /// <summary>
        /// 得到网页源代码
        /// </summary>
        /// <param name="strUrl">网址</param>
        /// <returns></returns>
        public string WebClientGetGeneralContent(string strUrl)
        {
            System.Net.WebClient MyWebClient = new System.Net.WebClient();


            MyWebClient.Credentials = System.Net.CredentialCache.DefaultCredentials;//获取或设置用于向Internet资源的请求进行身份验证的网络凭据

            Byte[] pageData = MyWebClient.DownloadData(strUrl); //从指定网站下载数据

            string strMsg = Encoding.Default.GetString(pageData);  //如果获取网站页面采用的是GB2312，则使用这句
            //string pageHtml = Encoding.UTF8.GetString(pageData); //如果获取网站页面采用的是UTF-8，则使用这句
            return strMsg;
        }

        /// <summary>
        /// 得到章节名
        /// </summary>
        /// <param name="strMsg"></param>
        /// <returns></returns>
        public string GetTitle(string strMsg)
        {
            string strTitle = string.Empty;
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(regTitle);
            System.Text.RegularExpressions.Match mc = reg.Match(strMsg);
            //while (mc.Success) { }
            strTitle = mc.Result("$1");
            strTitle += "\r\n";
            return strTitle;
        }
        /// <summary>
        /// 得到内容
        /// </summary>
        /// <param name="strMsg">网页源代码</param>
        /// <returns></returns>
        public string GetContent(string strMsg)
        {
            string strCon = string.Empty;
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(regContent);
            System.Text.RegularExpressions.Match mc = reg.Match(strMsg);
            //while (mc.Success) { }
            strCon = mc.Result("$1");
            return strCon;
        }
        public string GetNextWeb(string strMsg)
        {
            string strNext = string.Empty;
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(regNextWeb);
            System.Text.RegularExpressions.Match mc = reg.Match(strMsg);
            //while (mc.Success) { }
            strNext = mc.Result("$1");

            if (strNext == "") return strNext;

            Uri absolutePath1 = new Uri(nowWeb);
            Uri parseUrl = new Uri(absolutePath1, strNext);
            return parseUrl.ToString();

        }

        public string ReplaceContent(string strCon)
        {
            strCon = strCon.Replace(regEnter, "\r\n");
            strCon = strCon.Replace(regSpace, " ");
            strCon += "\r\n";
            return strCon;
        }
    }
}
