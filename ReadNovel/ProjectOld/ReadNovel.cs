﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadNovel
{
    public partial class ReadNovel : Form
    {
        public ReadNovel()
        {
            InitializeComponent();
        }

        private void ReadNovel_Load(object sender, EventArgs e)
        {
            txtMsg.Text = this.instructions();//说明
            txtSave.Text = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "novel.txt";//默认地址
        }

        //解锁 按钮
        private void btnUnlocked_Click(object sender, EventArgs e)
        {
            btnDowload.Enabled = false;
            txtStart.ReadOnly = false;
            txtEnd.ReadOnly = false;
        }

        //测试前两章
        //下载前两章内容到txtMsg中
        private void btnTest_Click(object sender, EventArgs e)
        {
            btnDowload.Enabled = true;
            txtStart.ReadOnly = true;
            txtEnd.ReadOnly = true;
            txtMsg.Clear();

            //判断地址是否正确
            string strUrl = txtStart.Text.Trim();
            strUrl = urlCheck(strUrl);
            if (strUrl == "")
            {
                MessageBox.Show("开始地址格式不正确！");
                return;
            }
            else
            {
                txtStart.Text = strUrl;
            }

            ReadNovelFromWeb rn = new ReadNovelFromWeb(strUrl);
            if(rn.isExistINI==0)
            {
                MessageBox.Show("未找到该网址的配置！");
                return;
            }
            //ReadNovelFromWeb rn = new ReadNovelFromWeb();
            //rn.startWeb = strUrl;
            //rn.nextWeb = rn.startWeb;

            for (int i = 1; i <= 2 && rn.nextWeb != "" && rn.nowWeb != txtEnd.Text.Trim(); i++)
            {
                rn.nowWeb = rn.nextWeb;
                rn.GetNovel(rn.nowWeb);

                txtMsg.Text += rn.novel.Title + "\r\n"; ;
                txtMsg.Text += rn.novel.Content;
            }
        }

        //下载
        private void btnDowload_Click(object sender, EventArgs e)
        {
            txtMsg.Clear();
            ReadNovelFromWeb rn = new ReadNovelFromWeb();
            rn.startWeb = txtStart.Text.Trim();
            rn.nextWeb = rn.startWeb;

            string path = txtSave.Text.Trim();
            FileStream fs;
            fs = new FileStream(path, FileMode.Append, FileAccess.Write);
            //StreamWriter sw = new StreamWriter(fs, Encoding.GetEncoding("gb2312"));
            StreamWriter sw = new StreamWriter(fs, Encoding.Default);

            DateTime start = DateTime.Now;
            txtMsg.Text += "开始：" + start + "\r\n";

            int i = 0;
            while (i <= 6000 && rn.nextWeb != "" && rn.nowWeb != txtEnd.Text.Trim())
            {
                rn.nowWeb = rn.nextWeb;
                rn.GetNovel(rn.nowWeb);

                sw.WriteLine(rn.novel.Title);//开始写入章节名
                sw.WriteLine(rn.novel.Content);//开始写入内容

                i++;
                txtMsg.Text += i + ". " + rn.novel.Title;

            }
            sw.Close();
            fs.Close();
            DateTime end = DateTime.Now;
            txtMsg.Text += "开始：" + start + "\r\n";
            txtMsg.Text += "结束：" + end + "\r\n";
            txtMsg.Text += "共 " + i + " 章\r\n";
            txtMsg.Text += "用时：" + (end - start).ToString() + "\r\n";
        }

        //程序说明
        private string instructions()
        {
            string str = "说明\r\n\r\n"
                + "开始网址：是下载小说的起始位置，建议为小说第一章的网址。\r\n"
                + "结束网址：是下载小说的结束位置。可以为空，为空则下载到最后一章。\r\n"
                + "保存位置：小说生成的TXT文件的位置。可以为空，默认地址为该程序目录下，文件名为novel.txt。\r\n\r\n"
                + "使用流程：\r\n"
                + "1.输入 开始网址\r\n"
                + "2.输入 结束网址（可以不输）\r\n"
                + "3.选择保存的txt位置\r\n"
                + "4.单击 测试前两章 进行测试，查看是否存在问题\r\n"
                + "5.单击 下载 进行下载小说\r\n\r\n"
                + "网游之射破苍穹的测试网址：http://www.bxwx.org/b/34/34780/5713222.html";
            return str;
        }

        //判断地址是否正确
        private string urlCheck(string strUrl)
        {
            //if (strUrl.IndexOf("://") < 0) strUrl = "http://" + strUrl;

            //匹配Url的正则表达式：
            //  ^[a-zA-z]+://[^\s]*$
            //  (https?://)?([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*/?
            //  [a-zA-z]+://([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*/?
            string regUrl = @"^[a-zA-z]+://([\da-z\.-]+)\.([a-z\.]{2,6})([/\w \.-]*)*/?$";
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(regUrl);
            System.Text.RegularExpressions.Match mc = reg.Match(strUrl);
            if (mc.Length <= 0)
            {
                strUrl = "http://" + strUrl;
            }

            try
            {
                Uri i = new Uri(strUrl);
                return i.ToString();
            }
            catch
            {
                return "";
            }
        }

        //保存位置
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            sfd.Filter = "文本文件(*.txt)|*.txt";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                txtSave.Text = sfd.FileName;
            }
        }
    }
}
