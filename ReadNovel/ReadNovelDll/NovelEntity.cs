﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Net;
using System.IO;
using NSoup.Nodes;

namespace ReadNovelDll
{
    [Serializable]
    [XmlRoot("Novel")]
    //[XmlRoot(Namespace = "", IsNullable = false,ElementName = "Novel")]
    public class NovelEntity
    {
        /// <summary>
        /// 目录网址
        /// </summary>
        public string Url { set; get; }
        /// <summary>
        /// 目录网页源码
        /// </summary>
        public string WebSource { set; get; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { set; get; }
        /// <summary>
        /// 章节列表
        /// </summary>
        public List<Catalog> Catalogs { set; get; }

        /// <summary>
        /// 内容所在标签
        /// </summary>
        public List<Sign> Signs { set; get; }

        private int _signUsed = 0;
        //[NonSerialized]
        /// <summary>
        /// 获取章节源码未下载的数量
        /// </summary>
        public int Undown
        { 
            get 
            { 
                int i = 0;
                foreach (Catalog item in Catalogs)
	            {
		            if(!item.IsDownload)i++;
	            }
                return i;
            }
        }

       
        /// <summary>
        /// 用于多线程
        /// </summary>
        private int finishcount = 0;
        private object locker = new object();

        public NovelEntity() 
        {    
            System.Net.ServicePointManager.DefaultConnectionLimit = 512;
            Catalogs = new List<Catalog>();
            Signs = new List<Sign>();
        }

        /// <summary>
        /// 1.初始化
        /// </summary>
        /// <param name="url"></param>
        public NovelEntity(string url):this()
        {

            Url = url;
            WebSource = webClientGetGeneralContent(Url);
            getEncoding();
            if (_encode != Encoding.UTF8)
            {
                WebSource = webClientGetGeneralContent(Url);
            }
        }

        
        
        /// <summary>
        /// 2.解析目录，生成章节列表
        /// </summary>
        public void ParserCatalog()
        {
            Document n = NSoup.NSoupClient.Parse(WebSource, Url);

            Title = n.GetElementsByTag("title").First.Text();

            Uri baseUrl = new Uri(Url);

            int j = 0;
            foreach (Element item in n.Select("a[href]"))
            {               
                string url = item.Attr("abs:href");
                if (IsUrl(url))
                {
                    Uri toUrl = new Uri(url);

                    /*
                   * 保证从目录网页中获取所有章节网址
                   * IsSpecialCase(baseUrl,toUrl) 排除特殊情况
                   * baseUrl.IsBaseOf(toUrl)  保证章节网址为下级网页（名称可能错误，大体意思是这个）
                   * !baseUrl.Equals(toUrl)   保证不是目录页
                   * baseUrl.MakeRelativeUri(toUrl).ToString()[0]!='#' 保证不是直到底部或顶部
                   *      eg:https://www.bqg5200.com/xiaoshuo/29/29993/  与 https://www.bqg5200.com/xiaoshuo/29/29993/#bottom
                   *  
                   * baseUrl.ToString().Replace("index.html", ".html").Length < toUrl.ToString().Length
                   *      保证下一级
                   *      eg:https://www.bxwx9.org/b/5/5740/index.html 与 "https://www.bxwx9.org/b/5/5740/41212429.html"
                   */
                    if (IsSpecialCase(baseUrl,toUrl) ||
                        (baseUrl.IsBaseOf(toUrl)
                        && !baseUrl.Equals(toUrl)
                        && baseUrl.MakeRelativeUri(toUrl).ToString()[0] != '#'
                        && baseUrl.ToString().Replace("index.html", ".html").Length < toUrl.ToString().Length
                        ))
                    {
                        Catalog c = new Catalog();
                        c.Id = j;
                        c.Url = toUrl.ToString();
                        c.title = item.Text();

                        Catalogs.Add(c);
                        j++;
                    }
                }
            }
           

        }

        /// <summary>
        /// 章节网址是否是特殊情况
        /// </summary>
        /// <param name="baseUri"></param>
        /// <param name="toUri"></param>
        /// <returns></returns>
        private bool IsSpecialCase(Uri baseUri, Uri toUri)
        {
            switch (baseUri.Host)
            {
                /*
                 * www.17k.com
                 * 目录网址为：http://www.17k.com/list/2843082.html
                 * 章节网址为：http://www.17k.com/chapter/2843082/35255044.html
                 */
                case "www.17k.com":
                    string url = baseUri.ToString().Replace("list", "chapter");
                    Uri u = new Uri(url);
                    return !u.Equals(toUri) && u.IsBaseOf(toUri);
                    
                default:
                    return false;
            }
        }


        /// <summary>
        /// 3.通过章节列表，下载章节源码
        /// </summary>
        public void Download()
        {
            for (int i = 0; i < Catalogs.Count; i++)
            {
                Thread trd = new Thread(new ParameterizedThreadStart(_download));
                trd.Start(i);
            }
            lock (locker)
            {
                while (finishcount != Catalogs.Count)
                {
                    Monitor.Wait(locker);//等待
                }
            }
        finishcount = 0;
        locker = new object();
            //Console.WriteLine("Thread Finished!");
        }

        private void _download(object i)
        {
            try
            {
                int index = (int)i;
                string uri = Catalogs[index].Url;
                //Console.WriteLine(uri);
                //Console.WriteLine(i);
                if (IsUrl(uri) && !Catalogs[index].IsDownload)
                {
                    #region 方式一 使用

                    Catalogs[index].SectionSnource = webClientGetGeneralContent(uri);
                    Catalogs[index].IsDownload = true;
                    #endregion

                    #region 方式二
                    //System.Net.WebClient MyWebClient = new System.Net.WebClient();
                    //MyWebClient.Credentials = System.Net.CredentialCache.DefaultCredentials;//获取或设置用于向Internet资源的请求进行身份验证的网络凭据                    
                    //Byte[] pageData = MyWebClient.DownloadData(uri); //从指定网站下载数据
                    ////string strMsg = Encoding.Default.GetString(pageData);  //如果获取网站页面采用的是GB2312，则使用这句
                    //Catalogs[index].SectioSnource = Encoding.UTF8.GetString(pageData);

                    ////Catalogs[index].SectioSnource = NovelO.WebClientGetGeneralContent(Url);
                    //Catalogs[index].IsDownload = true;
                    #endregion

                    #region 方式三

                    //WebRequest request = WebRequest.Create(uri); //WebRequest.Create方法，返回WebRequest的子类HttpWebRequest
                    //WebResponse response = request.GetResponse(); //WebRequest.GetResponse方法，返回对 Internet 请求的响应
                    //Stream resStream = response.GetResponseStream(); //WebResponse.GetResponseStream 方法，从 Internet 资源返回数据流。
                    //Encoding enc = Encoding.GetEncoding("utf-8"); // 如果是乱码就改成 utf-8 / GB2312
                    //StreamReader sr = new StreamReader(resStream, enc); //命名空间:System.IO。 StreamReader 类实现一个 TextReader (TextReader类，表示可读取连续字符系列的读取器)，使其以一种特定的编码从字节流中读取字符。
                    //Catalogs[index].SectioSnource = sr.ReadToEnd(); //输出(HTML代码)
                    //Catalogs[index].IsDownload = true; 
                    //resStream.Close();
                    //sr.Close();
                    #endregion

                }
            }
            catch (Exception)
            {

            }
            finally
            {
                lock (locker)
                {
                    finishcount++;
                    Monitor.Pulse(locker); //完成，通知等待队列,告知已完，执行下一个。
                }
            }
        }
        /// <summary>
        /// 判断一个字符串是否为url
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static bool IsUrl(string str)
        {
            try
            {
                string Url = @"^http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$";
                return Regex.IsMatch(str, Url);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 编码方式
        /// </summary>
        private Encoding _encode = Encoding.UTF8;
        /// <summary>
        /// 获取编码方式
        /// 根据网页的HTML内容提取网页的Encoding
        /// 来自 https://blog.csdn.net/a497785609/article/details/8234189
        /// </summary>
        private void getEncoding()
        {
            string pattern = @"(?i)\bcharset=(?<charset>[-a-zA-Z_0-9]+)";
            string charset = Regex.Match(WebSource, pattern).Groups["charset"].Value;
            try
            {
                _encode = Encoding.GetEncoding(charset);
            }
            catch (ArgumentException)
            {
                _encode = Encoding.UTF8;

            }
        }

        /// <summary>
        /// 获取网页源码
        /// </summary>
        /// <param name="strUrl"></param>
        /// <returns></returns>
        private string webClientGetGeneralContent1(string strUrl)
        {
            System.Net.WebClient MyWebClient = new System.Net.WebClient();
            MyWebClient.Credentials = System.Net.CredentialCache.DefaultCredentials;//获取或设置用于向Internet资源的请求进行身份验证的网络凭据
            Byte[] pageData = MyWebClient.DownloadData(strUrl); //从指定网站下载数据

            string strMsg = _encode.GetString(pageData);  //如果获取网站页面采用的是GB2312，则使用这句
            return strMsg;
        }

        private string webClientGetGeneralContent(string url)
        {
            string htmlCode;
            HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            webRequest.Timeout = 30000;
            webRequest.Method = "GET";
            webRequest.UserAgent = "Mozilla/4.0";
            webRequest.Headers.Add("Accept-Encoding", "gzip, deflate");


            HttpWebResponse webResponse = (System.Net.HttpWebResponse)webRequest.GetResponse();

            //获取目标网站的编码格式
            string contentype = webResponse.Headers["Content-Type"];
            Regex regex = new Regex("charset\\s*=\\s*[\\W]?\\s*([\\w-]+)", RegexOptions.IgnoreCase);
            if (webResponse.ContentEncoding.ToLower() == "gzip")//如果使用了GZip则先解压
            {
                using (System.IO.Stream streamReceive = webResponse.GetResponseStream())
                {
                    using (var zipStream = new System.IO.Compression.GZipStream(streamReceive, System.IO.Compression.CompressionMode.Decompress))
                    {

                        //匹配编码格式
                        if (regex.IsMatch(contentype))
                        {
                            Encoding ending = Encoding.GetEncoding(regex.Match(contentype).Groups[1].Value.Trim());
                            using (StreamReader sr = new System.IO.StreamReader(zipStream, ending))
                            {
                                htmlCode = sr.ReadToEnd();
                            }
                        }
                        else
                        {
                            using (StreamReader sr = new System.IO.StreamReader(zipStream, Encoding.UTF8))
                            {
                                htmlCode = sr.ReadToEnd();
                            }
                        }
                    }
                }
            }
            else
            {
                using (System.IO.Stream streamReceive = webResponse.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(streamReceive, Encoding.Default))
                    {
                        htmlCode = sr.ReadToEnd();
                    }
                }
            }
            return htmlCode;
        }


        /// <summary>
        ///  根据网页的HTML内容提取网页的Title
        ///  来自 https://blog.csdn.net/a497785609/article/details/8234189
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private static string getTitle(string html)
        {
            string pattern = @"(?si)<title(?:\s+(?:""[^""]*""|'[^']*'|[^""'>])*)?>(?<title>.*?)</title>";
            return Regex.Match(html, pattern).Groups["title"].Value.Trim();
        }

        private static bool isChina(string str)
        {
            int i = Convert.ToInt32(Convert.ToChar(str));
            if (i < 0x4E00 || i > 0x9fa5)
                return false;
            else
                return true;
        }

        /// <summary>
        /// 测试次数
        /// </summary>
        private int _CS = 10;
        /// <summary>
        /// (4)生成内容标志
        /// </summary>
        public void CreateSigns()
        {
            //生成内容标志
            int max = _CS;
            for (int i = 0; i < max && i<Catalogs.Count; i++)
            {
                if (Catalogs[i].IsDownload)
                {
                    try
                    {
                        SectionEntity s = new SectionEntity(Catalogs[i].SectionSnource);
                        s.Operate();
                        Signs.Add(s.SignHtml);
                    }
                    catch (Exception)
                    {
                        System.Diagnostics.Debug.WriteLine("❤❤1-2❤❤ NovelEntity.CreateSigns  "+i);
                        //throw;
                    }
                }
                else
                {
                    max++;
                }
            }

            //根据频率排序

            Signs.RemoveAll(x=> x.Id.Trim()==""&&x.Tag.Trim()==""&&x.Class.Trim()=="");
            Signs.ForEach(x =>
                {
                    if (x.Id != null && x.Id.Trim() != "")
                    { x.Class = ""; x.Tag = ""; }
                });


            var query = (from x in Signs
                         group x by new { x.Id, x.Class, x.Tag } into g
                         select new { g.Key.Id, g.Key.Class, g.Key.Tag, cs = g.Count() }
                         ).OrderByDescending(x => x.cs).ToList();
            List<Sign> list = new List<Sign>();
            foreach (var item in query)
            {
                list.Add(new Sign() { Id = item.Id, Class = item.Class, Tag = item.Tag });
            }
            Signs.Clear();
            Signs.AddRange(list);

        }

        /// <summary>
        /// 5.生成章节内容
        /// </summary>
        public void GetSection()
        {
            for (int i = 0; i < Catalogs.Count; i++)
            {
                Thread trd = new Thread(new ParameterizedThreadStart(_getSection));
                trd.Start(i);
            }
            lock (locker)
            {
                while (finishcount != Catalogs.Count)
                {
                    Monitor.Wait(locker);//等待
                }
            }
            finishcount = 0;
            locker = new object();
        }
        private void _getSection(object i)
        {
            try
            {
                int index = (int)i;
                if (Catalogs[index].IsDownload)
                {
                    string html = Catalogs[index].SectionSnource;
                    Catalogs[index].Section = SectionO.GetSectionContent(html, Signs[_signUsed]);                    
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("❤❤1-5❤❤ NovelEntity._getSection  i" + i.ToString()+"   --- msg:"+ex.Message);
            }
            finally
            {
                lock (locker)
                {
                    finishcount++;
                    Monitor.Pulse(locker); //完成，通知等待队列,告知已完，执行下一个。
                }
            }
        }


        public string GetSectionByIndex(int index)
        {
            if (index >= Catalogs.Count) return "";
            string html = Catalogs[index].SectionSnource;
            return Catalogs[index].title + "\n" + SectionO.GetSectionContent(html, Signs[_signUsed]);  
        }

        /// <summary>
        /// （6）.重新下载乱码的网页源码
        /// </summary>
        public void DealErrorCode()
        {
            foreach (Catalog c in Catalogs)
            {
                if (c.Section == null || c.Section.Length < 50)
                {
                    string html = webClientGetGeneralContent(c.Url);
                    c.SectionSnource = html;
                    c.Section = SectionO.GetSectionContent(html, Signs[_signUsed]);

                    System.Diagnostics.Debug.WriteLine("❤❤1-3❤❤ NovelEntity.DealErrorCode  " + Catalogs.IndexOf(c));
                }
            }
        }


        /// <summary>
        /// 7.保存成TXT文件
        /// </summary>
        public void SaveTXT(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Create);
            StreamWriter wr = new StreamWriter(fs);
            try
            {
                wr.WriteLine(Title);
                wr.WriteLine();
                wr.WriteLine();
                foreach (var item in Catalogs)
                {
                    wr.WriteLine(item.title);
                    wr.WriteLine();
                    wr.WriteLine(item.Section);
                    wr.WriteLine();
                }
            }
            catch (Exception)
            {


            }
            finally
            {
                wr.Close();
            }
        }


        public override string ToString()
        {
            return "Url:" + Url + "； WebSource:" + WebSource + "。";
        }

    }
    [Serializable]
    public class Catalog
    {
        /// <summary>
        /// 编号，生成章节列表时的顺序
        /// </summary>
        public int Id { set; get; }
        /// <summary>
        /// 章节网址
        /// </summary>
        public string Url { set; get; }
        /// <summary>
        /// 目录页上的章节标题
        /// </summary>
        public string title { set; get; }
        /// <summary>
        /// 是否已下载章节网页源码
        /// </summary>
        public bool IsDownload { set; get; }
        /// <summary>
        /// 章节网页源码
        /// </summary>
        public string SectionSnource { set; get; }
        /// <summary>
        /// 章节内容
        /// </summary>
        public string Section { set; get; }

        public Catalog()
        {
            IsDownload = false;
        }
    }
}
