﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ReadNovelDll
{
    public static class XmlO
    {
        /// <summary>
        /// 实体类序列化成xml string
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static string ToXMLString<T>(T entity)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                XmlTextWriter writer = new XmlTextWriter(stream, null);
                XmlSerializer xml = new XmlSerializer(typeof(T));
                xml.Serialize(writer, entity);
                writer.Formatting = Formatting.Indented;
                using (StreamReader sr = new StreamReader(stream, System.Text.Encoding.UTF8))
                {
                    stream.Position = 0;
                    string xmlString = sr.ReadToEnd();
                    sr.Close();
                    stream.Close();
                    return xmlString;
                }
            }
        }

        /// <summary>
        /// 实体类反序列化
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T ReadFromXML<T>(string xml)
        {
            T entity;
            byte[] byts = Encoding.UTF8.GetBytes(xml);
            using (MemoryStream stream = new MemoryStream(byts))
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                entity = (T)xs.Deserialize(stream);
                return entity;
            }
        }

        /// <summary>
        /// 序列化为XML文件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public static void ToXMLFile<T>(T entity)
        {
            XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
            TextWriter textwriter = new StreamWriter(CreateFileName());
            xmlserializer.Serialize(textwriter, entity);
            textwriter.Close();
        }
        /// <summary>
        /// 序列化为XML文件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="path">文件路径</param>
        public static void ToXMLFile<T>(T entity,string path)
        {
            XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
            TextWriter textwriter = new StreamWriter(path);
            xmlserializer.Serialize(textwriter, entity);
            textwriter.Close();
        }

        /// <summary>
        /// 生成XML文件名
        /// </summary>
        /// <returns></returns>
        private static string CreateFileName()
        {
            string name = DateTime.Now.ToString("yyyyMMddHHmmss");
            if (!File.Exists(name+".xml"))
            {
                return name+".xml";
            }
            for (int i = 1; i < 100; i++)
			{
                string n = name + "_" + i.ToString("00") + ".xml";
                if (!File.Exists(name + ".xml"))
                {
                    return n;
                }
			}
            return name + ".xml";
        }

        /// <summary>
        /// 读取XML文件，转化为实体类
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T ToEntity<T>(string path)
        {
            if (!File.Exists(path))
            {
                return default(T);
            }
            else
            {            
                XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
                FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read);
                StreamReader textwriter = new StreamReader(fs);
                T entity = (T)xmlserializer.Deserialize(textwriter);
                textwriter.Close();
                fs.Close();
                return entity;


                //FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read);
                //StreamReader sr = new StreamReader(fs);
                //string text = sr.ReadToEnd();

                //XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
                //StreamReader textwriter = new StreamReader(path);
                //return (T)xmlserializer.Deserialize(textwriter);
            }
        }
    }
}
