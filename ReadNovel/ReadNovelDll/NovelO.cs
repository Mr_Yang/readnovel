﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ReadNovelDll
{
    public static class NovelO
    {
        /// <summary>
        /// get方法  根据URL地址获取网页信息
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetUrltoHtml(string Url, string type)
        {
            try
            {
                System.Net.WebRequest wReq = System.Net.WebRequest.Create(Url);
                // Get the response instance.
                System.Net.WebResponse wResp = wReq.GetResponse();
                System.IO.Stream respStream = wResp.GetResponseStream();
                // Dim reader As StreamReader = New StreamReader(respStream)
                using (System.IO.StreamReader reader = new System.IO.StreamReader(respStream, Encoding.GetEncoding(type)))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (System.Exception ex)
            {
                //errorMsg = ex.Message;
            }
            return "";
        }

        /////<summary>
        /////采用https协议访问网络
        /////</summary>
        /////<param name="URL">url地址</param>
        /////<param name="strPostdata">发送的数据</param>
        /////<returns></returns>
        //public string OpenReadWithHttps(string URL, string strPostdata, string strEncoding)
        //{
        //    Encoding encoding = Encoding.Default;
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
        //    request.Method = "post";
        //    request.Accept = "text/html, application/xhtml+xml, */*";
        //    request.ContentType = "application/x-www-form-urlencoded";
        //    byte[] buffer = encoding.GetBytes(strPostdata);
        //    request.ContentLength = buffer.Length;
        //    request.GetRequestStream().Write(buffer, 0, buffer.Length);
        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    using (StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.GetEncoding(strEncoding)))
        //    {
        //        return reader.ReadToEnd();
        //    }
        //}

         /// <summary>
        /// 过滤html标签
        /// </summary>
        /// <param name="strHtml">html的内容</param>
        /// <returns></returns>
        public static string StripHTML(string stringToStrip)
        {
            // paring using RegEx           //
            stringToStrip = Regex.Replace(stringToStrip, "</p(?:\\s*)>(?:\\s*)<p(?:\\s*)>", "\n\n", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            stringToStrip = Regex.Replace(stringToStrip, "", "\n", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            stringToStrip = Regex.Replace(stringToStrip, "\"", "''", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            stringToStrip = StripHtmlXmlTags(stringToStrip);
            return stringToStrip;
        }
 
        private static string StripHtmlXmlTags(string content)
        {
            return Regex.Replace(content, "<[^>]+>", "", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        #region 转化 URL
 
        //public static string URLDecode(string text)
        //{
        //    return HttpUtility.UrlDecode(text, Encoding.Default);
        //}
 
        //public static string URLEncode(string text)
        //{
        //    return HttpUtility.UrlEncode(text, Encoding.Default);
        //}
 
        #endregion

        ///<summary>
        /// 请求的公共类用来向服务器发送请求
        ///</summary>
        ///<param name="strSMSRequest">发送请求的字符串</param>
        ///<returns>返回的是请求的信息</returns>
        private static string SMSrequest(string strSMSRequest)
        {
            byte[] data = new byte[1024];
            string stringData = null;
            System.Net.IPHostEntry gist = System.Net.Dns.GetHostByName("www.110.cn");
            System.Net.IPAddress ip = gist.AddressList[0];
            //得到IP 
            System.Net.IPEndPoint ipEnd = new System.Net.IPEndPoint(ip, 3121);
            //默认80端口号 
            System.Net.Sockets.Socket socket = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
            //使用tcp协议 stream类型 
            try
            {
                socket.Connect(ipEnd);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                return "Fail to connect server\r\n" + ex.ToString();
            }
            string path = strSMSRequest.ToString().Trim();
            StringBuilder buf = new StringBuilder();
            //buf.Append("GET ").Append(path).Append(" HTTP/1.0\r\n");
            //buf.Append("Content-Type: application/x-www-form-urlencoded\r\n");
            //buf.Append("\r\n");
            byte[] ms = System.Text.UTF8Encoding.UTF8.GetBytes(buf.ToString());
            //提交请求的信息
            socket.Send(ms);
            //接收返回 
            string strSms = "";
            int recv = 0;
            do
            {
                recv = socket.Receive(data);
                stringData = Encoding.ASCII.GetString(data, 0, recv);
                //如果请求的页面meta中指定了页面的encoding为gb2312则需要使用对应的Encoding来对字节进行转换() 
                strSms = strSms + stringData;
                //strSms += recv.ToString();
            }
            while (recv != 0);
            socket.Shutdown(System.Net.Sockets.SocketShutdown.Both);
            socket.Close();
            return strSms;
        }

        /// <summary>
        /// 得到网页源代码
        /// </summary>
        /// <param name="strUrl">网址</param>
        /// <returns></returns>
        public static string WebClientGetGeneralContent(string strUrl)
        {
            System.Net.WebClient MyWebClient = new System.Net.WebClient();


            MyWebClient.Credentials = System.Net.CredentialCache.DefaultCredentials;//获取或设置用于向Internet资源的请求进行身份验证的网络凭据

            Byte[] pageData = MyWebClient.DownloadData(strUrl); //从指定网站下载数据

            string strMsg = Encoding.Default.GetString(pageData);  //如果获取网站页面采用的是GB2312，则使用这句
            //string strMsg = Encoding.UTF8.GetString(pageData); //如果获取网站页面采用的是UTF-8，则使用这句
            return strMsg;
        }


    }
}
