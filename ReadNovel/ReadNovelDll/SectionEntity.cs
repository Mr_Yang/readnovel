﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSoup.Nodes;

namespace ReadNovelDll
{
    public class SectionEntity
    {
        /// <summary>
        /// 章节网页源码
        /// </summary>
        public string WebSource { set; get; }

        private Document doc { set; get; }

        /// <summary>
        /// 解析列表
        /// </summary>
        public List<HtmlParse> HtmlParses { set; get; }
        /// <summary>
        /// 内容所在标签
        /// </summary>
        public Sign SignHtml { set; get; }

        public SectionEntity(string webSource)
        {
            WebSource = webSource;
            //Nodes = new List<Node>();
            HtmlParses = new List<HtmlParse>();
            SignHtml = new Sign();
            //Nodes.AddRange(list);
            Operate();
        }

        /// <summary>
        /// 获取内容所在的标识
        /// </summary>
        public Sign Operate()
        {
            GetNodesList();
            RemoveUnrelated();
            GetSign();
            return SignHtml;
        }

        /// <summary>
        /// 获取Node的列表，包括所有父项及子项
        /// </summary>
        private void GetNodesList()
        {
            doc = NSoup.NSoupClient.Parse(WebSource);
            doc.GetElementsByTag("head").Remove();
            doc.GetElementsByTag("script").Remove();
            doc.GetElementsByTag("img").Remove();
            doc.GetElementsByTag("a").Remove();

            NodeTraverse(doc.ChildNodes, 1);
        }
        /// <summary>
        /// 遍历，获取Node的列表
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="cheng"></param>
        private void NodeTraverse(IList<Node> nodes, int cheng)
        {
            foreach (Node item in nodes)
            {
                HtmlParses.Add(new HtmlParse() { N = item, NodeName = item.NodeName, ChenCi = cheng, Length = item.ToString().Length });
                if (item.ChildNodes.Count > 0)
                {
                    NodeTraverse(item.ChildNodes, ++cheng);
                }
            }
        }

        /// <summary>
        /// 删除无关的Node
        /// </summary>
        private void RemoveUnrelated()
        {
            HtmlParses.RemoveAll(x => (x.Length < 300));

            //计算汉字个数
            CalculateWord();
            HtmlParses.RemoveAll(x => (x.HanZi < 300));
            HtmlParses.Sort((x, y) => { return -x.HanZi.CompareTo(y.HanZi); });          

        }

        private void CalculateWord()
        {
            foreach (HtmlParse item in HtmlParses)
            {
                int count = 0;
                //Regex regex = new Regex(@"^[\u4E00-\u9FA5]{0,}$");
                string str = item.N.ToString();
                for (int i = 0; i < str.Length; i++)
                {
                    //regex.IsMatch(str[i].ToString())
                    int temp = Convert.ToInt32(str[i]);
                    if (temp >= 0x4E00 && temp<= 0x9fa5)
                    {
                        count++;
                    }
                }
                item.HanZi = count;
            }
        }

        private void GetSign()
        {
            int keyPoint = 0;
            for (int i = 1; i < HtmlParses.Count; i++)
            {
                if (HtmlParses[i - 1].HanZi * 1.0 / HtmlParses[i].HanZi > 1.5)//66%
                {
                    keyPoint = i;
                    break;
                }
            }
            if (keyPoint > 0) HtmlParses.RemoveAll(x => HtmlParses.IndexOf(x) >= keyPoint);

            Node n = HtmlParses[HtmlParses.Count - 1].N;

            SignHtml.Tag = n.NodeName;
            SignHtml.Id = n.Attributes.ContainsKey("id") ? n.Attributes["id"] : "";
            SignHtml.Class = n.Attributes.ContainsKey("class") ? n.Attributes["class"] : "";

            //if(n.Attributes.ContainsKey("id")) SignHtml.Id = n.Attributes["id"];
            //if(n.Attributes.ContainsKey("class")) SignHtml.Class = n.Attributes["class"];


            //n.Attributes
            //Document d = new Document("d");
            //d.GetElementsByAttribute(_

        }
    }

    public class HtmlParse
    {
        public Node N { set; get; }

        public int ChenCi { set; get; }

        public string NodeName { set; get; }
        public int Length { set; get; }
        public int HanZi { set; get; }

        public override string ToString()
        {
            return ChenCi.ToString() + " " + NodeName + " " + HanZi.ToString() + "---" + Length.ToString() + " " + N.ToString();
        }
    }

    public class Sign
    {
        public string Id { set; get; }
        public string Tag { set; get; }
        public string Class { set; get; }
    }
}
