﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSoup.Nodes;
using System.Text.RegularExpressions;

namespace ReadNovelDll
{
    public static class SectionO
    {
        public static string GetSectionContent(string source, Sign signHtml)
        {
            Document doc = NSoup.NSoupClient.Parse(source);
            //doc.GetElementsByTag("br").re
            string content =null;
            if(signHtml.Id !=null && signHtml.Id.Trim()!="")
            {
                content = doc.GetElementById(signHtml.Id).OuterHtml();
            }
            else if (signHtml.Class != null && signHtml.Class.Trim() != "")
            {
                content = doc.GetElementsByClass(signHtml.Class).Where(x => x.Tag.Name == signHtml.Tag).FirstOrDefault().OuterHtml();
            }
            if (content == null)
            {
                SectionEntity se = new SectionEntity(source);
                se.Operate();
                content = se.HtmlParses[se.HtmlParses.Count - 1].N.OuterHtml();

                System.Diagnostics.Debug.WriteLine("❤❤1-1❤❤ SectionO.GetSectionContent");
            }
            content = RemoveHTML(content);
            return content;
        }

        public static string RemoveHTML(string Htmlstring)
        {
            //去除空格及回车
            Htmlstring = Regex.Replace(Htmlstring, @" ", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"\n", "", RegexOptions.IgnoreCase);
            //显示回车
            Htmlstring = Regex.Replace(Htmlstring, @"[\n]?<\s*br\s*/?\s*>[\n]?", "\r\n", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<\s*/\s*p\s*>", "\r\n", RegexOptions.IgnoreCase);
            //删除脚本   
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML   
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            //Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);

            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", "   ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);

            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            //Htmlstring.Replace("\r\n", "");
            //Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim();

            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n]\s*){5,}", "\r\n", RegexOptions.IgnoreCase);
            return Htmlstring;
        }
    }

}
